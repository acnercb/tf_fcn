from ext_imports import *

# Savd color dictionary: maps class names (i.e. 'person' or 'dog') 
# to RGB pixels (i.e. a 3-tuple of numbers) in the mask images
color_dict = pickle.load(open("segmentation_color_codes.pkl", 'rb'))
color_dict_list = list(color_dict.items())
color_dict_array = np.transpose(np.array([item[1] for item in color_dict_list]))

def labels_to_mask(labels):
    """ turns 1-channel labels image (s) into 3-channel RGB mask image (s) 
    input: 3-D np array (single image) or 4-D np array (an array of 2-D images)
            (a 2-D labeled image is stored in a 3-D array because labels
            are one-hot encoded)
    output: same type and shape as input
    """
    if len(labels.shape) == 3:
        tiled = np.tile(labels[:,:,None,:], (1,1,3,1))
    elif len(labels.shape) == 4:
        tiled = np.tile(labels[:,:,:,None,:], (1,1,1,3,1))
    mask21 = tiled*color_dict_array
    mask = np.sum(mask21, axis=-1)  
    
    return mask
    
def mask_to_labels(mask):
    """ turns 3-channel RGB mask image (s) into 1-channel labels image (s)
    input: 3-D np array (single image) or 4-D np array (an array of 2-D images)
    output: same type and shape as input (labels are one-hot encoded)
    """
    if len(mask.shape) == 3:
        labels = np.all(np.equal(mask[:,:,:,None], color_dict_array), axis=2)
    elif len(mask.shape) == 4:
        labels = np.all(np.equal(mask[:,:,:,:,None], color_dict_array), axis=3)
        
    return labels