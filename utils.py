from ext_imports import *
from load_weights import get_vgg_assign_ops
# Paths
data_home_path = "/home/ubuntu/tf_fcn/data/TrainVal/VOCdevkit/VOC2011/" 
data_image_path = "JPEGImages/"
data_mask_path = "SegmentationClass/"
segmentation_file_list = "ImageSets/Segmentation/"

# Segmentation file lists
train_files = "train.txt"
val_files = "val.txt"

# Segmentation image file names
training_names_path = data_home_path+segmentation_file_list+train_files
with open(training_names_path, 'r') as file:
    training_file_list = file.read()
    training_file_list = [name for name in training_file_list.split('\n') if len(name)>0]

# Segmentation training and mask images
training_file_paths = [data_home_path+data_image_path+file_name+".jpg" for file_name in training_file_list]
training_mask_paths = [data_home_path+data_mask_path+file_name+".png" for file_name in training_file_list]

# Iterators
iter_images = iter(training_file_paths)
iter_masks  = iter(training_mask_paths)